﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/ActionResponses/ChangeRoom")]
public class ChangeRoomResponse : ActionResponse
{
	public Room m_roomToChangeTo;

	public override bool DoActionResponse(GameController gameController)
	{
		if (IsThisTheCurrentRoomForAnAction(gameController))
		{
			gameController.SetNewCurrentRoom(m_roomToChangeTo);
			gameController.DisplayRoomText();
			return true;
		}

		return false;
	}

	private bool IsThisTheCurrentRoomForAnAction(GameController gameController)
	{
		return gameController.GetCurrentRoomName() == m_requiredString;
	}
}
