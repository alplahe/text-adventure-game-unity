﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Examine")]
public class Examine : InputAction
{
    public override void RespondToInput(GameController gameController, string[] separatedInputWords)
    {
        string verb = separatedInputWords[0];
        string noun = separatedInputWords[1];
        gameController.LogStringWithReturn(gameController.TestVerbDictionaryWithNoun(
            gameController.GetExamineDictionary(), verb, noun));
    }
}
