﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Exit
{
    public string m_keyString;
    public string m_exitDescription;
    public Room m_valueRoom;
}
