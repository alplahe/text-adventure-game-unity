﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	public Text m_displayText;
	public InputAction[] m_inputActions;
	
	[HideInInspector] public RoomNavigation m_roomNavigation;
	[HideInInspector] public List<string> m_interactionDescriptionsInRoom = new List<string>();
	[HideInInspector] public InteractableItems m_interactableItems;
	
	List<string> m_actionLog = new List<string>();

	// Use this for initialization
	private void Awake ()
	{
		m_roomNavigation = GetComponent<RoomNavigation>();
		m_interactableItems = GetComponent<InteractableItems>();
	}
	
	private void Start ()
	{
		DisplayRoomText();
		DisplayLoggedText();
	}
	
	public void DisplayRoomText()
	{
		ClearCollectionsForNewRoom();
		
		UnpackRoom();

		string joinedInteractionDescriptions = string.Join("\n", m_interactionDescriptionsInRoom.ToArray());
		
		string combinedText = m_roomNavigation.m_currentRoom.m_description + "\n" + joinedInteractionDescriptions;
		
		LogStringWithReturn(combinedText);
	}

	private void UnpackRoom()
	{
		m_roomNavigation.UnpackExitsInRoom();
		
		PrepareObjectsToTakeOrExamine(m_roomNavigation.m_currentRoom);
	}

	private void PrepareObjectsToTakeOrExamine(Room currentRoom)
	{
		SearchInteractableObjectsInRoom(currentRoom);
	}

	private void SearchInteractableObjectsInRoom(Room currentRoom)
	{
		for (int i = 0; i < currentRoom.m_interactableObjectsInRoom.Length; i++)
		{
			IfDescriptionNotInInventoryAddToInteractionDescriptionsInRoom(currentRoom, i);

			SearchInteractionsInObject(currentRoom, i);
		}
	}

	private void IfDescriptionNotInInventoryAddToInteractionDescriptionsInRoom(Room currentRoom, int i)
	{
		string descriptionNotInInventory = m_interactableItems.GetObjectsNotInInventory(currentRoom, i);
		if (descriptionNotInInventory != null)
		{
			AddDescriptionNotInInventoryToInteractionDescriptionsInRoom(descriptionNotInInventory);
		}
	}

	private void SearchInteractionsInObject(Room currentRoom, int i)
	{
		InteractableObject interactableInRoom = currentRoom.m_interactableObjectsInRoom[i];

		for (int j = 0; j < interactableInRoom.m_interactions.Length; j++)
		{
			string interactableInRoomNoun = interactableInRoom.m_noun;
			Interaction interaction = interactableInRoom.m_interactions[j];
			string interactionKeyWord = interaction.m_inputAction.m_keyWord;
			string interactionTextResponse = interaction.m_textResponse;

			if (interactionKeyWord == "examine")
			{
				AddInteractableInRoomNounAndTextResponseToExamineDictionary(interactableInRoomNoun, interactionTextResponse);
			}

			if (interactionKeyWord == "take")
			{
				AddInteractableInRoomNounAndTextResponseToTakeDictionary(interactableInRoomNoun, interactionTextResponse);
			}
		}
	}

	private void AddInteractableInRoomNounAndTextResponseToExamineDictionary(string interactableInRoomNoun,
		string interactionTextResponse)
	{
		m_interactableItems.m_examineDictionary.Add(interactableInRoomNoun, interactionTextResponse);
	}

	private void AddInteractableInRoomNounAndTextResponseToTakeDictionary(string interactableInRoomNoun,
		string interactionTextResponse)
	{
		m_interactableItems.m_takeDictionary.Add(interactableInRoomNoun, interactionTextResponse);
	}

	private void AddDescriptionNotInInventoryToInteractionDescriptionsInRoom(string descriptionNotInInventory)
	{
		m_interactionDescriptionsInRoom.Add(descriptionNotInInventory);
	}

	public string TestVerbDictionaryWithNoun(Dictionary<string, string> verbDictionary, string verb, string noun)
	{
		if (IsNounInVerbDictionary(verbDictionary, noun))
		{
			return verbDictionary[noun];
		}

		return ReturnNounNotFoundErrorMessage(verb, noun);
	}

	private bool IsNounInVerbDictionary(Dictionary<string, string> verbDictionary, string noun)
	{
		return verbDictionary.ContainsKey(noun);
	}

	private string ReturnNounNotFoundErrorMessage(string verb, string noun)
	{
		return "You can't " + verb + " " + noun;
	}

	private void ClearCollectionsForNewRoom()
	{
		m_interactableItems.ClearCollections();
		m_interactionDescriptionsInRoom.Clear();
		m_roomNavigation.ClearExits();
	}

	public void LogStringWithReturn(string stringToAdd)
	{
		m_actionLog.Add(stringToAdd + "\n");
	}

	public void DisplayLoggedText()
	{
		string logAsText = string.Join("\n", m_actionLog.ToArray());

		m_displayText.text = logAsText;
	}

	public void AttemptToChangeRooms(string directionNoun)
	{
		m_roomNavigation.AttemptToChangeRooms(directionNoun);
	}
	
	public Dictionary<string, string> TakeInteractableItem(string[] separatedInputWords)
	{
		return m_interactableItems.Take(separatedInputWords);
	}

	public Dictionary<string, string> GetExamineDictionary()
	{
		return m_interactableItems.m_examineDictionary;
	}

	public void DisplayInventory()
	{
		m_interactableItems.DisplayInventory();
	}

	public string GetCurrentRoomName()
	{
		return m_roomNavigation.GetName();
	}

	public void SetNewCurrentRoom(Room room)
	{
		m_roomNavigation.m_currentRoom = room;
	}

	public void UseItem(string[] separatedInputWords)
	{
		m_interactableItems.UseItem(separatedInputWords);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
