﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "TextAdventure/InputActions/Go")]
public class Go : InputAction
{
	public override void RespondToInput(GameController gameController, string[] separatedInputWords)
	{
		string direction = separatedInputWords[1];		//north, south, east or west
		gameController.AttemptToChangeRooms(direction);
	}
}
