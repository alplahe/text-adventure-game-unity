﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputAction : ScriptableObject
{
	public string m_keyWord;

	public abstract void RespondToInput(GameController gameController, string[] separatedInputWords);
}
