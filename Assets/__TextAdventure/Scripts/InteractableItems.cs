﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InteractableItems : MonoBehaviour
{
	public List<InteractableObject> m_usableItemList;
	
	public Dictionary<string, string> m_examineDictionary = new Dictionary<string, string>();
	public Dictionary<string, string> m_takeDictionary = new Dictionary<string, string>();

	[HideInInspector] public List<string> m_nounsInRoom = new List<string>();
	
	private Dictionary<string, ActionResponse> m_useDictionary = new Dictionary<string, ActionResponse>();
	private List<string> m_nounsInInventory = new List<string>();
	private GameController m_gameController;

	private void Awake()
	{
		m_gameController = GetComponent<GameController>();
	}

	public string GetObjectsNotInInventory(Room currentRoom, int i)
	{
		InteractableObject interactableInRoom = currentRoom.m_interactableObjectsInRoom[i];

		if (IsNounInRoomNotInInventory(interactableInRoom))
		{
			AddNounInRoom(interactableInRoom.m_noun);
			
			return interactableInRoom.m_description;
		}

		return null;
	}

	private bool IsNounInRoomNotInInventory(InteractableObject interactableInRoom)
	{
		return !m_nounsInInventory.Contains(interactableInRoom.m_noun);
	}

	private void AddNounInRoom(string noun)
	{
		m_nounsInRoom.Add(noun);
	}

	public void AddActionResponsesToUseDictionary()
	{
		SearchInteractableObjectsFromUsableList();
	}

	private void SearchInteractableObjectsFromUsableList()
	{
		for (int i = 0; i < m_nounsInInventory.Count; i++)
		{
			string noun = m_nounsInInventory[i];

			InteractableObject interactableObjectInInventory = GetInteractableObjectFromUsableList(noun);
			if (interactableObjectInInventory == null)
			{
				continue;
			}

			SearchInteractionsInInteractableObjectInInventory(interactableObjectInInventory, noun);
		}
	}

	private void SearchInteractionsInInteractableObjectInInventory(InteractableObject interactableObjectInInventory,
		string noun)
	{
		for (int j = 0; j < interactableObjectInInventory.m_interactions.Length; j++)
		{
			Interaction interaction = interactableObjectInInventory.m_interactions[j];
			if (interaction.m_actionResponse == null)
			{
				continue;
			}

			IfNounIsNotInUseDictionaryAddActionResponseToUseDictionary(noun, interaction);
		}
	}

	private void IfNounIsNotInUseDictionaryAddActionResponseToUseDictionary(string noun, Interaction interaction)
	{
		if (!m_useDictionary.ContainsKey(noun))
		{
			AddNounAndInteractionActionResponseToUseDictionary(noun, interaction);
		}
	}

	private void AddNounAndInteractionActionResponseToUseDictionary(string noun, Interaction interaction)
	{
		m_useDictionary.Add(noun, interaction.m_actionResponse);
	}

	private InteractableObject GetInteractableObjectFromUsableList(string noun)
	{
		for (int i = 0; i < m_usableItemList.Count; i++)
		{
			if (IsNounInUsableItemList(noun, i))
			{
				return m_usableItemList[i];
			}
		}
		return null;
	}

	private bool IsNounInUsableItemList(string noun, int i)
	{
		return m_usableItemList[i].m_noun == noun;
	}

	public void DisplayInventory()
	{
		m_gameController.LogStringWithReturn("You look in your backpack, inside you have: ");

		DisplayOneItemPerLine();
	}

	private void DisplayOneItemPerLine()
	{
		for (int i = 0; i < m_nounsInInventory.Count; i++)
		{
			string noun = m_nounsInInventory[i];
			m_gameController.LogStringWithReturn(noun);
		}
	}

	public void ClearCollections()
	{
		m_examineDictionary.Clear();
		m_takeDictionary.Clear();
		m_nounsInRoom.Clear();
	}

	public Dictionary<string, string> Take(string[] separatedInputWords)
	{
		string noun = separatedInputWords[1];

		if (IsNounInRoom(noun))
		{
			AddNounToInventory(noun);
			AddActionResponsesToUseDictionary();
			RemoveNounFromRoom(noun);
			return m_takeDictionary;
		}
		else
		{
			LogNounNotInRoomErrorMessage(noun);
			return null;
		}
	}

	private bool IsNounInRoom(string noun)
	{
		return m_nounsInRoom.Contains(noun);
	}

	private void AddNounToInventory(string noun)
	{
		m_nounsInInventory.Add(noun);
	}

	private void RemoveNounFromRoom(string noun)
	{
		m_nounsInRoom.Remove(noun);
	}

	private void LogNounNotInRoomErrorMessage(string noun)
	{
		m_gameController.LogStringWithReturn("There is no " + noun + " here to take.");
	}

	public void UseItem(string[] separatedInputWords)
	{
		string nounToUse = separatedInputWords[1];

		IfNounIsInInventory(nounToUse);
		IfNounIsNotInInventoryThenLogErrorMessage(nounToUse);
	}

	private void IfNounIsInInventory(string nounToUse)
	{
		if (IsNounInInventory(nounToUse))
		{
			IfNounIsInUseDictionary(nounToUse);
			IfNounIsNotInUseDictionaryThenLogErrorMessage(nounToUse);
		}
	}

	private bool IsNounInInventory(string nounToUse)
	{
		return m_nounsInInventory.Contains(nounToUse);
	}

	private void IfNounIsInUseDictionary(string nounToUse)
	{
		if (IsNounInUseDictionary(nounToUse))
		{
			bool actionResult = GetActionResultAndDoThatAction(nounToUse);
			
			IfActionHasNoResultThenLogErrorMessage(actionResult);
		}
	}

	private bool IsNounInUseDictionary(string nounToUse)
	{
		return m_useDictionary.ContainsKey(nounToUse);
	}

	private bool GetActionResultAndDoThatAction(string nounToUse)
	{
		bool actionResult = m_useDictionary[nounToUse].DoActionResponse(m_gameController);
		return actionResult;
	}

	private void IfActionHasNoResultThenLogErrorMessage(bool actionResult)
	{
		if (!actionResult)
		{
			m_gameController.LogStringWithReturn("Hmm. Nothing happens.");
		}
	}

	private void IfNounIsNotInUseDictionaryThenLogErrorMessage(string nounToUse)
	{
		if (IsNounNotInUseDictionary(nounToUse))
		{
			m_gameController.LogStringWithReturn("You can't use the " + nounToUse + ".");
		}
	}

	private bool IsNounNotInUseDictionary(string nounToUse)
	{
		return !m_useDictionary.ContainsKey(nounToUse);
	}

	private void IfNounIsNotInInventoryThenLogErrorMessage(string nounToUse)
	{
		if (IsNounNotInInventory(nounToUse))
		{
			m_gameController.LogStringWithReturn("There is no " + nounToUse + " in your inventory to use.");
		}
	}

	private bool IsNounNotInInventory(string nounToUse)
	{
		return !m_nounsInInventory.Contains(nounToUse);
	}
}
