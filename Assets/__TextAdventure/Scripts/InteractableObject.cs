﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/Interactable Object")]
public class InteractableObject : ScriptableObject
{
	public string m_noun = "name";
	[TextArea]
	public string m_description = "Description in room";

	public Interaction[] m_interactions;
}
