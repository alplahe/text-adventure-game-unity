﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Interaction
{
    public InputAction m_inputAction;
    [TextArea] 
    public string m_textResponse;
    public ActionResponse m_actionResponse;
}
