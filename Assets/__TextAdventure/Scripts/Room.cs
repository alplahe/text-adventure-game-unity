﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/Room")]
public class Room : ScriptableObject
{
    [TextArea]
    public string m_description;
    public string m_roomName;
    public Exit[] m_exits;
    public InteractableObject[] m_interactableObjectsInRoom;
}
