﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomNavigation : MonoBehaviour
{
    public Room m_currentRoom;

    Dictionary<string,Room> m_exitDictionary = new Dictionary<string, Room>();
    private GameController m_gameController;

    private void Awake()
    {
        m_gameController = GetComponent<GameController>();
    }

    public void UnpackExitsInRoom()
    {
        for (int i = 0; i < m_currentRoom.m_exits.Length; i++)
        {
            AddExitCharacteristicsToDictionaryAndList(i);
        }
    }

    private void AddExitCharacteristicsToDictionaryAndList(int i)
    {
        string exitKeyStringOfCurrentRoom = m_currentRoom.m_exits[i].m_keyString;
        Room exitValueRoomOfCurrentRoom = m_currentRoom.m_exits[i].m_valueRoom;
        string exitDescriptionOfCurrentRoom = m_currentRoom.m_exits[i].m_exitDescription;

        AddExitKeyStringAndValueRoomToDictionary(exitKeyStringOfCurrentRoom, exitValueRoomOfCurrentRoom);
        AddExitDescriptionToInteractionDescriptionsInRoomList(exitDescriptionOfCurrentRoom);
    }

    private void AddExitKeyStringAndValueRoomToDictionary(string exitKeyStringInCurrentRoom, Room exitValueRoomInCurrentRoom)
    {
        m_exitDictionary.Add(exitKeyStringInCurrentRoom, exitValueRoomInCurrentRoom);
    }

    private void AddExitDescriptionToInteractionDescriptionsInRoomList(string exitDescriptionOfCurrentRoom)
    {
        m_gameController.m_interactionDescriptionsInRoom.Add(exitDescriptionOfCurrentRoom);
    }

    public void AttemptToChangeRooms(string directionNoun)
    {
        if (m_exitDictionary.ContainsKey(directionNoun))
        {
            CanChangeRooms(directionNoun);
        }
        else
        {
            CannotChangeRooms(directionNoun);
        }
    }

    private void CanChangeRooms(string directionNoun)
    {
        SetNewCurrentRoomFromDictionary(directionNoun);
        LogNewPathFromDictionary(directionNoun);
        m_gameController.DisplayRoomText();
    }
    
    private void CannotChangeRooms(string directionNoun)
    {
        m_gameController.LogStringWithReturn("There is no path to the " +
                                             directionNoun); // Ex: There is no path to the north
    }

    private void SetNewCurrentRoomFromDictionary(string directionNoun)
    {
        m_currentRoom = m_exitDictionary[directionNoun];
    }

    private void LogNewPathFromDictionary(string directionNoun)
    {
        m_gameController.LogStringWithReturn("You head off to the " + directionNoun); // Ex: You head off to the north
    }

    public void ClearExits()
    {
        m_exitDictionary.Clear();
    }

    public string GetName() 
    {
        return m_currentRoom.m_roomName;
    }
}