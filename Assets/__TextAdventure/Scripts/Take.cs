﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Take")]
public class Take : InputAction
{
    public override void RespondToInput(GameController gameController, string[] separatedInputWords)
    {
        Dictionary<string, string> takeDictionary = gameController.TakeInteractableItem(separatedInputWords);

        if (takeDictionary != null)
        {
            string verb = separatedInputWords[0];
            string noun = separatedInputWords[1];
            gameController.LogStringWithReturn(gameController.TestVerbDictionaryWithNoun(
                takeDictionary, verb, noun));
        }
    }
}
