﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour
{
	public InputField m_inputField;
	
	private GameController m_gameController;

	private void Awake()
	{
		m_gameController = GetComponent<GameController>();
		m_inputField.onEndEdit.AddListener(AcceptStringInput);
	}

	private void AcceptStringInput(string userInput)
	{
		LogLowerStringWithReturn(userInput);

		AnalyzeUserInput(userInput);

		DisplayLoggedText();
		ResetInputFieldText();
	}

	private void LogLowerStringWithReturn(string userInput)
	{
		userInput = userInput.ToLower();
		m_gameController.LogStringWithReturn(userInput);
	}

	private void AnalyzeUserInput(string userInput)
	{
		string[] separatedInputWords = SplitInputWords(userInput);
		string verbFromInputWords = separatedInputWords[0];

		SearchVerbFromInputInInputActionsArray(verbFromInputWords, separatedInputWords);
	}
	
	private string[] SplitInputWords(string userInput)
	{
		char[] delimiterCharacters = {' '};
		string[] separatedInputWords = userInput.Split(delimiterCharacters);
		return separatedInputWords;
	}

	private void SearchVerbFromInputInInputActionsArray(string verbFromInputWords, string[] separatedInputWords)
	{
		for (int i = 0; i < m_gameController.m_inputActions.Length; i++)
		{
			InputAction inputAction = m_gameController.m_inputActions[i];
			IsVerbFromInputAKeyWord(inputAction, verbFromInputWords, separatedInputWords);
		}
	}

	private void IsVerbFromInputAKeyWord(InputAction inputAction, string verbFromInputWords, string[] separatedInputWords)
	{
		if (inputAction.m_keyWord == verbFromInputWords)
		{
			inputAction.RespondToInput(m_gameController, separatedInputWords);
		}
	}

	private void DisplayLoggedText()
	{
		m_gameController.DisplayLoggedText();
	}

	private void ResetInputFieldText()
	{
		m_inputField.ActivateInputField();
		m_inputField.text = null;
	}
}
